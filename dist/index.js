var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SceneLoadTest = /** @class */ (function (_super) {
    __extends(SceneLoadTest, _super);
    function SceneLoadTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    SceneLoadTest.prototype.init = function () {
        var view3D = new feng3d.View();
        feng3d.loader.loadText("resources/scene/Untitled.scene.json", function (content) {
            var json = JSON.parse(content);
            var sceneobject = feng3d.serialization.deserialize(json);
            var scene = sceneobject.getComponent(feng3d.Scene);
            view3D.scene = scene;
        });
    };
    /**
     * 更新
     */
    SceneLoadTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    SceneLoadTest.prototype.dispose = function () {
    };
    return SceneLoadTest;
}(feng3d.Script));
var Basic_Shading = /** @class */ (function (_super) {
    __extends(Basic_Shading, _super);
    function Basic_Shading() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    Basic_Shading.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var planeMaterial;
        var sphereMaterial;
        var cubeMaterial;
        var torusMaterial;
        var light1;
        var light2;
        var plane;
        var sphere;
        var cube;
        var torus;
        initEngine();
        initLights();
        initMaterials();
        initObjects();
        initListeners();
        function initEngine() {
            camera.node3d.y = 5;
            camera.node3d.z = -10;
            camera.node3d.lookAt(new feng3d.Vector3());
            camera.node3d.addComponent(feng3d.FPSController);
        }
        function initMaterials() {
            planeMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_specular.jpg" } },
                }
            });
            sphereMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/beachball_diffuse.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/beachball_specular.jpg" } },
                }
            });
            cubeMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_specular.jpg" } },
                }
            });
            torusMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_diffuse.jpg" } },
                }
            });
        }
        function initLights() {
            scene.ambientColor.a = 0.2;
            light1 = new feng3d.Entity().addComponent(feng3d.DirectionalLight);
            light1.intensity = 0.7;
            light1.node3d.rx = 90;
            scene.node3d.addChild(light1.node3d);
            light2 = new feng3d.Entity().addComponent(feng3d.DirectionalLight);
            light2.color.fromUnit(0x00FFFF);
            light2.intensity = 0.7;
            light2.node3d.rx = 90;
            scene.node3d.addChild(light2.node3d);
        }
        function initObjects() {
            plane = new feng3d.Entity().addComponent(feng3d.Renderable);
            var geometry = plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 10, height: 10 });
            plane.material = planeMaterial;
            geometry.scaleU = 2;
            geometry.scaleV = 2;
            plane.node3d.y = -0.20;
            scene.node3d.addChild(plane.node3d);
            sphere = new feng3d.Entity().addComponent(feng3d.Renderable);
            sphere.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 1.50, segmentsW: 40, segmentsH: 20 });
            sphere.material = sphereMaterial;
            sphere.node3d.x = 3;
            sphere.node3d.y = 1.60;
            sphere.node3d.z = 3.00;
            scene.node3d.addChild(sphere.node3d);
            cube = new feng3d.Entity().addComponent(feng3d.Renderable);
            cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 2, height: 2, depth: 2, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
            cube.material = cubeMaterial;
            cube.node3d.x = 3.00;
            cube.node3d.y = 1.60;
            cube.node3d.z = -2.50;
            scene.node3d.addChild(cube.node3d);
            torus = new feng3d.Entity().addComponent(feng3d.Renderable);
            geometry = torus.geometry = feng3d.serialization.setValue(new feng3d.TorusGeometry(), { radius: 1.50, tubeRadius: 0.60, segmentsR: 40, segmentsT: 20 });
            torus.material = torusMaterial;
            geometry.scaleU = 10;
            geometry.scaleV = 5;
            torus.node3d.x = -2.50;
            torus.node3d.y = 1.60;
            torus.node3d.z = -2.50;
            scene.node3d.addChild(torus.node3d);
        }
        function initListeners() {
            feng3d.ticker.onframe(onEnterFrame, this);
        }
        function onEnterFrame() {
            light1.node3d.rx = 30;
            light1.node3d.ry++;
        }
    };
    /**
     * 更新
     */
    Basic_Shading.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    Basic_Shading.prototype.dispose = function () {
    };
    return Basic_Shading;
}(feng3d.Script));
var Basic_SkyBox = /** @class */ (function (_super) {
    __extends(Basic_SkyBox, _super);
    function Basic_SkyBox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    Basic_SkyBox.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var cubeTexture = feng3d.serialization.setValue(new feng3d.TextureCube(), {
            rawData: {
                type: "path", paths: [
                    'resources/skybox/snow_positive_x.jpg',
                    'resources/skybox/snow_positive_y.jpg',
                    'resources/skybox/snow_positive_z.jpg',
                    'resources/skybox/snow_negative_x.jpg',
                    'resources/skybox/snow_negative_y.jpg',
                    'resources/skybox/snow_negative_z.jpg',
                ]
            }
        });
        var skybox = feng3d.serialization.setValue(new feng3d.Entity(), { name: "skybox" }).addComponent(feng3d.SkyBox);
        skybox.s_skyboxTexture = cubeTexture;
        scene.node3d.addChild(skybox.node3d);
        camera.node3d.z = -6;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.lens = new feng3d.PerspectiveLens(90);
        var torusMaterial = feng3d.serialization.setValue(new feng3d.Material(), { uniforms: { s_envMap: cubeTexture } });
        // torusMaterial.uniforms.u_specular.a = 0.5;
        // torusMaterial.uniforms.u_ambient.fromUnit(0x111111);
        // torusMaterial.uniforms.u_ambient.a = 0.25;
        var torus = feng3d.serialization.setValue(new feng3d.Entity(), { name: "torus" }).addComponent(feng3d.Renderable);
        torus.geometry = feng3d.serialization.setValue(new feng3d.TorusGeometry(), { radius: 1.50, tubeRadius: 0.60, segmentsR: 40, segmentsT: 20 });
        torus.material = torusMaterial;
        scene.node3d.addChild(torus.node3d);
        feng3d.ticker.onframe(function () {
            torus.node3d.rx += 2;
            torus.node3d.ry += 1;
            camera.node3d.x = 0;
            camera.node3d.y = 0;
            camera.node3d.z = 0;
            camera.node3d.ry += 0.5 * (feng3d.windowEventProxy.clientX - canvas.clientLeft - canvas.clientWidth / 2) / 800;
            camera.node3d.moveBackward(6);
        });
    };
    /**
     * 更新
     */
    Basic_SkyBox.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    Basic_SkyBox.prototype.dispose = function () {
    };
    return Basic_SkyBox;
}(feng3d.Script));
var Basic_View = /** @class */ (function (_super) {
    __extends(Basic_View, _super);
    function Basic_View() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    Basic_View.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        camera.node3d.z = -6;
        camera.node3d.y = 5;
        camera.node3d.lookAt(new feng3d.Vector3());
        var plane = new feng3d.Entity().addComponent(feng3d.Renderable);
        plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 7, height: 7 });
        var material = plane.material = feng3d.serialization.setValue(new feng3d.Material(), { uniforms: { s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_diffuse.jpg" } } } });
        scene.node3d.addChild(plane.node3d);
        feng3d.ticker.onframe(function () {
            plane.node3d.ry += 1;
        });
    };
    /**
     * 更新
     */
    Basic_View.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    Basic_View.prototype.dispose = function () {
    };
    return Basic_View;
}(feng3d.Script));
var BillboardTest = /** @class */ (function (_super) {
    __extends(BillboardTest, _super);
    function BillboardTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /*
     * 初始化时调用
     */
    BillboardTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        camera.node3d.addComponent(feng3d.FPSController);
        scene.background.setTo(0.3, 0.3, 0.3, 1);
        var cube = feng3d.Entity.createPrimitive("Cube");
        cube.z = 3;
        scene.node3d.addChild(cube);
        var model = feng3d.Entity.createPrimitive("Plane").addComponent(feng3d.Renderable);
        model.node3d.y = 1.50;
        var holdSizeComponent = model.addComponent(feng3d.HoldSizeComponent);
        holdSizeComponent.holdSize = 1;
        holdSizeComponent.camera = camera;
        var billboardComponent = model.addComponent(feng3d.BillboardComponent);
        billboardComponent.camera = camera;
        cube.addChild(model.node3d);
        //材质
        model.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 0.1, height: 0.1, segmentsW: 1, segmentsH: 1, yUp: false });
        var textureMaterial = model.material = feng3d.serialization.setValue(new feng3d.Material(), { uniforms: { s_diffuse: { __class__: "feng3d.Texture2D", source: { url: 'resources/m.png' } } } });
        // textureMaterial.cullFace = CullFace.NONE;
        //
        // var texture = textureMaterial.texture = new ImageDataTexture();
        // var canvas2D = document.createElement("canvas");
        // canvas2D.width = 256;
        // canvas2D.height = 256;
        // var context2D = canvas2D.getContext("2d");
        // // context2D.fillStyle = "red";
        // // context2D.fillRect(0, 0, canvas2D.width, canvas2D.height);
        // context2D.fillStyle = "green";
        // context2D.font = '48px serif';
        // // context2D.fillText('Hello world', 50, 100);
        // context2D.fillText('Hello world', 0, 50);
        // // context2D.strokeText('Hello world', 50, 100);
        // var imageData = context2D.getImageData(0, 0, canvas2D.width, canvas2D.height);
        // texture.pixels = imageData;
        // gameObject.holdSize = 1;
    };
    /**
     * 更新
     */
    BillboardTest.prototype.update = function () {
    };
    /**
     * 销毁时调用
     */
    BillboardTest.prototype.dispose = function () {
    };
    return BillboardTest;
}(feng3d.Script));
/**
 * 测试3D容器
 */
var Container3DTest = /** @class */ (function (_super) {
    __extends(Container3DTest, _super);
    function Container3DTest() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.num = 0;
        return _this;
    }
    /**
     * 初始化时调用
     */
    Container3DTest.prototype.init = function () {
        //初始化颜色材质
        this.cube = feng3d.Entity.createPrimitive("Cube");
        this.node3d.addChild(this.cube);
        this.colorMaterial = this.cube.getComponent(feng3d.Renderable).material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color" });
        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = 2;
        this.cube.addChild(cylinder);
    };
    /**
     * 更新
     */
    Container3DTest.prototype.update = function () {
        console.log("update");
        //变化旋转与颜色
        this.cube.ry += 1;
        this.num++;
        if (this.num % 60 == 0) {
            this.colorMaterial.uniforms.u_diffuseInput.fromUnit(Math.random() * (1 << 32 - 1));
        }
    };
    /**
     * 销毁时调用
     */
    Container3DTest.prototype.dispose = function () {
    };
    return Container3DTest;
}(feng3d.Script));
var FogTest = /** @class */ (function (_super) {
    __extends(FogTest, _super);
    function FogTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    FogTest.prototype.init = function () {
        var model = new feng3d.Entity().addComponent(feng3d.Renderable);
        model.node3d.z = -7;
        model.node3d.y = 0;
        this.node3d.addChild(model.node3d);
        model.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        //材质
        var material = model.material = feng3d.serialization.setValue(new feng3d.Material(), {
            uniforms: {
                s_diffuse: {
                    __class__: "feng3d.Texture2D",
                    source: { url: 'resources/m.png' }
                },
                u_fogMode: feng3d.FogMode.LINEAR,
                u_fogColor: new feng3d.Color3(1, 1, 0),
                u_fogMinDistance: 2,
                u_fogMaxDistance: 3,
            }
        });
        feng3d.ticker.onframe(function () {
            model.node3d.ry += 1;
        });
    };
    /**
     * 更新
     */
    FogTest.prototype.update = function () {
    };
    /**
     * 销毁时调用
     */
    FogTest.prototype.dispose = function () {
    };
    return FogTest;
}(feng3d.Script));
var FPSControllerTest = /** @class */ (function (_super) {
    __extends(FPSControllerTest, _super);
    function FPSControllerTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    FPSControllerTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var cube = feng3d.Entity.createPrimitive("Cube");
        this.node3d.addChild(cube);
        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        this.node3d.addChild(sphere);
        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        this.node3d.addChild(capsule);
        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        this.node3d.addChild(cylinder);
        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        //
        camera.node3d.addComponent(feng3d.FPSController);
    };
    /**
     * 更新
     */
    FPSControllerTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    FPSControllerTest.prototype.dispose = function () {
    };
    return FPSControllerTest;
}(feng3d.Script));
var MousePickTest = /** @class */ (function (_super) {
    __extends(MousePickTest, _super);
    function MousePickTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    MousePickTest.prototype.init = function () {
        /**
         * 操作方式:鼠标按下后可以使用移动鼠标改变旋转，wasdqe平移
         */
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);
        var cube = feng3d.Entity.createPrimitive("Cube");
        cube.mouseEnabled = true;
        cube.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(cube);
        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        sphere.mouseEnabled = true;
        sphere.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(sphere);
        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        capsule.mouseEnabled = true;
        capsule.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(capsule);
        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        cylinder.mouseEnabled = true;
        cylinder.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(cylinder);
        scene.on("click", function (event) {
            var entity = event.target;
            if (entity.getComponent(feng3d.Renderable)) {
                var uniforms = entity.getComponent(feng3d.Renderable).material.uniforms;
                uniforms.u_diffuse.fromUnit(Math.random() * (1 << 24));
            }
        });
        // var engines = feng3d.Feng3dObject.getObjects(feng3d.Engine);
        // engines[0].mouse3DManager.mouseInput.catchMouseMove = true;
        // scene.on("mouseover", (event) =>
        // {
        //     var gameObject = <feng3d.Entity>event.target;
        //     if (gameObject.getComponent(feng3d.Renderable))
        //     {
        //         var uniforms = <feng3d.StandardUniforms>gameObject.getComponent(feng3d.Renderable).material.uniforms;
        //         uniforms.u_diffuse.setTo(0, 1, 0);
        //     }
        // });
        // scene.on("mouseout", (event) =>
        // {
        //     var gameObject = <feng3d.Entity>event.target;
        //     if (gameObject.getComponent(feng3d.Renderable))
        //     {
        //         var uniforms = <feng3d.StandardUniforms>gameObject.getComponent(feng3d.Renderable).material.uniforms;
        //         uniforms.u_diffuse.setTo(1, 1, 1);
        //     }
        // });
    };
    /**
     * 更新
     */
    MousePickTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    MousePickTest.prototype.dispose = function () {
    };
    return MousePickTest;
}(feng3d.Script));
var ScriptTest = /** @class */ (function (_super) {
    __extends(ScriptTest, _super);
    function ScriptTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    ScriptTest.prototype.init = function () {
        var sc = this.entity.addScript("ScriptDemo");
    };
    /**
     * 更新
     */
    ScriptTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    ScriptTest.prototype.dispose = function () {
    };
    return ScriptTest;
}(feng3d.Script));
var SkyBoxTest = /** @class */ (function (_super) {
    __extends(SkyBoxTest, _super);
    function SkyBoxTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    SkyBoxTest.prototype.init = function () {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);
        //
        var skybox = feng3d.serialization.setValue(new feng3d.Entity(), { name: "skybox" }).addComponent(feng3d.SkyBox);
        skybox.s_skyboxTexture = feng3d.serialization.setValue(new feng3d.TextureCube(), {
            rawData: {
                type: "path", paths: [
                    'resources/skybox/px.jpg',
                    'resources/skybox/py.jpg',
                    'resources/skybox/pz.jpg',
                    'resources/skybox/nx.jpg',
                    'resources/skybox/ny.jpg',
                    'resources/skybox/nz.jpg'
                ]
            }
        });
        scene.node3d.addChild(skybox.node3d);
    };
    /**
     * 更新
     */
    SkyBoxTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    SkyBoxTest.prototype.dispose = function () {
    };
    return SkyBoxTest;
}(feng3d.Script));
var GeometryTest = /** @class */ (function (_super) {
    __extends(GeometryTest, _super);
    function GeometryTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    GeometryTest.prototype.init = function () {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var model = new feng3d.Entity().addComponent(feng3d.Renderable);
        var geometry = model.geometry = new feng3d.CustomGeometry();
        geometry.addGeometry(new feng3d.PlaneGeometry());
        var matrix = new feng3d.Matrix4x4();
        matrix.appendTranslation(0, 0.50, 0);
        geometry.addGeometry(feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 50 }), matrix);
        matrix.appendTranslation(0, 0.50, 0);
        var addGeometry = new feng3d.CubeGeometry();
        geometry.addGeometry(addGeometry, matrix);
        addGeometry.width = 0.50;
        matrix.appendTranslation(0, 0.50, 0);
        matrix.appendRotation(feng3d.Vector3.Z_AXIS, 45);
        geometry.addGeometry(addGeometry, matrix);
        model.node3d.z = 3;
        model.node3d.y = -1;
        scene.node3d.addChild(model.node3d);
        //初始化颜色材质
        model.material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color" });
        var colorUniforms = model.material.uniforms;
        //变化旋转与颜色
        setInterval(function () {
            model.node3d.ry += 1;
        }, 15);
        setInterval(function () {
            colorUniforms.u_diffuseInput.fromUnit(Math.random() * (1 << 32 - 1));
        }, 1000);
    };
    /**
     * 更新
     */
    GeometryTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    GeometryTest.prototype.dispose = function () {
    };
    return GeometryTest;
}(feng3d.Script));
var PrimitiveTest = /** @class */ (function (_super) {
    __extends(PrimitiveTest, _super);
    function PrimitiveTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    PrimitiveTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var cube = feng3d.Entity.createPrimitive("Cube");
        this.node3d.addChild(cube);
        var plane = feng3d.Entity.createPrimitive("Plane");
        plane.x = 1.50;
        plane.rx = -90;
        plane.sx = 0.1;
        plane.sy = 0.1;
        plane.sz = 0.1;
        this.node3d.addChild(plane);
        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        this.node3d.addChild(sphere);
        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        this.node3d.addChild(capsule);
        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        this.node3d.addChild(cylinder);
        var controller = new feng3d.LookAtController(camera.node3d);
        controller.lookAtPosition = new feng3d.Vector3();
        //
        setInterval(function () {
            var time = new Date().getTime();
            var angle = (Math.round(time / 17) % 360);
            angle = angle * Math.DEG2RAD;
            camera.node3d.x = 10 * Math.sin(angle);
            camera.node3d.y = 0;
            camera.node3d.z = 10 * Math.cos(angle);
            controller.update();
        }, 17);
    };
    /**
     * 更新
     */
    PrimitiveTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    PrimitiveTest.prototype.dispose = function () {
    };
    return PrimitiveTest;
}(feng3d.Script));
var PointLightTest = /** @class */ (function (_super) {
    __extends(PointLightTest, _super);
    function PointLightTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    PointLightTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var light0 = feng3d.serialization.setValue(new feng3d.Entity(), { name: "pointLight" }).addComponent(feng3d.Renderable);
        var light1 = feng3d.serialization.setValue(new feng3d.Entity(), { name: "pointLight" }).addComponent(feng3d.Renderable);
        initObjects();
        initLights();
        feng3d.ticker.onframe(setPointLightPosition);
        camera.node3d.z = -5;
        camera.node3d.y = 2;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);
        //
        feng3d.windowEventProxy.on("keyup", function (event) {
            var boardKey = String.fromCharCode(event.data.keyCode).toLocaleLowerCase();
            switch (boardKey) {
                case "c":
                    clearObjects();
                    break;
                case "b":
                    initObjects();
                    scene.node3d.addChild(light0.node3d);
                    scene.node3d.addChild(light1.node3d);
                    break;
            }
        });
        function initObjects() {
            var material = feng3d.serialization.setValue(new feng3d.Material(), {
                uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_diffuse.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_normals.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_specular.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                }
            });
            //初始化立方体
            var plane = new feng3d.Entity().addComponent(feng3d.Renderable);
            plane.node3d.y = -1;
            var geometry = plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 10, height: 10 });
            geometry.scaleU = 2;
            geometry.scaleV = 2;
            plane.material = material;
            scene.node3d.addChild(plane.node3d);
            var cube = new feng3d.Entity().addComponent(feng3d.Renderable);
            cube.material = material;
            cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
            cube.geometry.scaleU = 2;
            cube.geometry.scaleV = 2;
            scene.node3d.addChild(cube.node3d);
        }
        function clearObjects() {
            for (var i = scene.node3d.numChildren - 1; i >= 0; i--) {
                scene.node3d.removeChildAt(i);
            }
        }
        function initLights() {
            //
            var lightColor0 = new feng3d.Color4(1, 0, 0, 1);
            light0.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 0.05 });
            //初始化点光源
            var pointLight0 = light0.addComponent(feng3d.PointLight);
            pointLight0.color = lightColor0.toColor3();
            light0.material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color", uniforms: { u_diffuseInput: lightColor0 } });
            scene.node3d.addChild(light0.node3d);
            //
            var lightColor1 = new feng3d.Color4(0, 1, 0, 1);
            light1.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 0.05 });
            //初始化点光源
            var pointLight1 = light1.addComponent(feng3d.DirectionalLight);
            pointLight1.color = lightColor1.toColor3();
            light1.material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color", uniforms: { u_diffuseInput: lightColor1 } });
            scene.node3d.addChild(light1.node3d);
        }
        function setPointLightPosition() {
            var time = new Date().getTime();
            //
            var angle = time / 1000;
            light0.node3d.x = Math.sin(angle) * 3;
            light0.node3d.z = Math.cos(angle) * 3;
            //
            angle = angle + Math.PI / 2;
            light1.node3d.x = Math.sin(angle) * 3;
            light1.node3d.z = Math.cos(angle) * 3;
            light1.node3d.lookAt(new feng3d.Vector3());
        }
    };
    /**
     * 更新
     */
    PointLightTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    PointLightTest.prototype.dispose = function () {
    };
    return PointLightTest;
}(feng3d.Script));
var ColorMaterialTest = /** @class */ (function (_super) {
    __extends(ColorMaterialTest, _super);
    function ColorMaterialTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    ColorMaterialTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var cube = feng3d.Entity.createPrimitive("Cube");
        cube.z = 3;
        scene.node3d.addChild(cube);
        //初始化颜色材质
        var colorMaterial = cube.getComponent(feng3d.Renderable).material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color" });
        //变化旋转与颜色
        setInterval(function () {
            cube.ry += 1;
        }, 15);
        setInterval(function () {
            colorMaterial.uniforms.u_diffuseInput.fromUnit(Math.random() * (1 << 32 - 1));
        }, 1000);
    };
    /**
     * 更新
     */
    ColorMaterialTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    ColorMaterialTest.prototype.dispose = function () {
    };
    return ColorMaterialTest;
}(feng3d.Script));
var PointMaterialTest = /** @class */ (function (_super) {
    __extends(PointMaterialTest, _super);
    function PointMaterialTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    PointMaterialTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var pointGeometry = new feng3d.PointGeometry();
        var pointMaterial = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "point", renderParams: { renderMode: feng3d.RenderMode.POINTS } });
        var model = feng3d.serialization.setValue(new feng3d.Entity(), { name: "plane" }).addComponent(feng3d.Renderable);
        model.geometry = pointGeometry;
        model.material = pointMaterial;
        model.node3d.z = 3;
        scene.node3d.addChild(model.node3d);
        var length = 200;
        var height = 2 / Math.PI;
        for (var x = -length; x <= length; x = x + 4) {
            var angle = x / length * Math.PI;
            var vec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
            var pointInfo = new feng3d.PointInfo();
            pointInfo.position = vec;
            pointGeometry.points.push(pointInfo);
        }
        //变化旋转
        setInterval(function () {
            model.node3d.ry += 1;
            pointMaterial.uniforms.u_PointSize = 1 + 5 * Math.sin(model.node3d.ry / 30);
        }, 15);
    };
    /**
     * 更新
     */
    PointMaterialTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    PointMaterialTest.prototype.dispose = function () {
    };
    return PointMaterialTest;
}(feng3d.Script));
var SegmentMaterialTest = /** @class */ (function (_super) {
    __extends(SegmentMaterialTest, _super);
    function SegmentMaterialTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    SegmentMaterialTest.prototype.init = function () {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var segment = feng3d.serialization.setValue(new feng3d.Entity(), { name: "segment" }).addComponent(feng3d.Renderable);
        segment.node3d.z = 3;
        scene.node3d.addChild(segment.node3d);
        //初始化材质
        segment.material = feng3d.Material.getDefault("Segment-Material");
        var segmentGeometry = segment.geometry = new feng3d.SegmentGeometry();
        var length = 200;
        var height = 2 / Math.PI;
        var preVec;
        for (var x = -length; x <= length; x++) {
            var angle = x / length * Math.PI;
            if (preVec == null) {
                preVec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
            }
            else {
                var vec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
                segmentGeometry.addSegment({ start: preVec, end: vec });
                preVec = vec;
            }
        }
        //变化旋转
        setInterval(function () {
            segment.node3d.ry += 1;
        }, 15);
    };
    /**
     * 更新
     */
    SegmentMaterialTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    SegmentMaterialTest.prototype.dispose = function () {
    };
    return SegmentMaterialTest;
}(feng3d.Script));
var StandardMaterialTest = /** @class */ (function (_super) {
    __extends(StandardMaterialTest, _super);
    function StandardMaterialTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    StandardMaterialTest.prototype.init = function () {
        var scene = this.node3d.scene;
        ;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = 3;
        cube.node3d.y = -1;
        scene.node3d.addChild(cube.node3d);
        //变化旋转与颜色
        setInterval(function () {
            cube.node3d.ry += 1;
        }, 15);
        cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        // model.geometry = new PlaneGeometry();
        //材质
        var textureMaterial = cube.material = new feng3d.Material();
        var uniforms = textureMaterial.uniforms;
        uniforms.s_diffuse = new feng3d.Texture2D();
        uniforms.s_diffuse.source = { url: 'resources/m.png' };
        // textureMaterial.uniforms.s_diffuse.url = 'resources/nonpowerof2.png';
        uniforms.s_diffuse.format = feng3d.TextureFormat.RGBA;
        // textureMaterial.diffuseMethod.alphaThreshold = 0.1;
        uniforms.s_diffuse.anisotropy = 16;
        uniforms.u_diffuse.a = 0.2;
        textureMaterial.renderParams.enableBlend = true;
    };
    /**
     * 更新
     */
    StandardMaterialTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    StandardMaterialTest.prototype.dispose = function () {
    };
    return StandardMaterialTest;
}(feng3d.Script));
var TextureMaterialTest = /** @class */ (function (_super) {
    __extends(TextureMaterialTest, _super);
    function TextureMaterialTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * 初始化时调用
     */
    TextureMaterialTest.prototype.init = function () {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");
        var cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = 3;
        cube.node3d.y = -1;
        scene.node3d.addChild(cube.node3d);
        //变化旋转与颜色
        setInterval(function () {
            cube.node3d.ry += 1;
        }, 15);
        cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        // model.geometry = new PlaneGeometry();
        //材质
        cube.material = feng3d.serialization.setValue(new feng3d.Material(), {
            shaderName: "texture",
            uniforms: {
                s_texture: {
                    __class__: "feng3d.Texture2D", source: { url: 'resources/m.png' }, flipY: false
                }
            }
        });
    };
    /**
     * 更新
     */
    TextureMaterialTest.prototype.update = function () {
    };
    /**
    * 销毁时调用
    */
    TextureMaterialTest.prototype.dispose = function () {
    };
    return TextureMaterialTest;
}(feng3d.Script));
var ScriptDemo = /** @class */ (function (_super) {
    __extends(ScriptDemo, _super);
    function ScriptDemo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ScriptDemo.prototype.init = function () {
        var cube = this.cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = -7;
        this.node3d.addChild(cube.node3d);
        cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        //材质
        var material = cube.material = new feng3d.Material();
        var uniforms = material.uniforms;
        uniforms.s_diffuse = new feng3d.Texture2D();
        uniforms.s_diffuse.source = { url: 'resources/m.png' };
        uniforms.u_fogMode = feng3d.FogMode.LINEAR;
        uniforms.u_fogColor = new feng3d.Color3(1, 1, 0);
        uniforms.u_fogMinDistance = 2;
        uniforms.u_fogMaxDistance = 3;
    };
    ScriptDemo.prototype.update = function () {
        this.cube.node3d.ry += 1;
        // log("this.cube.ry: " + this.cube.ry);
    };
    /**
     * 销毁
     */
    ScriptDemo.prototype.dispose = function () {
        this.cube.dispose();
        this.cube = null;
    };
    return ScriptDemo;
}(feng3d.Script));
var GeometryFontTest = /** @class */ (function (_super) {
    __extends(GeometryFontTest, _super);
    function GeometryFontTest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GeometryFontTest.prototype.init = function () {
        var camera = this.node3d.scene.getComponentsInChildren(feng3d.Camera)[0];
        camera.addComponent(feng3d.FPSController);
        var cube = this.cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = -7;
        cube.node3d.x = -1;
        this.node3d.addChild(cube.node3d);
        //材质
        var material = cube.material = new feng3d.Material();
        material.renderParams.frontFace = feng3d.FrontFace.CCW;
        material.renderParams.cullFace = feng3d.CullFace.NONE;
        var script = document.createElement('script');
        script.onload = function (ev) {
            opentype.load('./resources/fonts/NotoSansCJKsc_Regular.otf', function (err, font) {
                if (err) {
                    alert('Font could not be loaded: ' + err);
                }
                else {
                    var fontData = extractFontData(font);
                    var contoursInfo = convert(fontData);
                    var font1 = new feng3d.Font(contoursInfo);
                    font1.isCCW = !!font['isCIDFont'];
                    // const { vertices, normals, uvs, indices } = font1.calculateGeometry('图', 1);
                    // const { vertices, normals, uvs, indices } = font1.calculateGeometry('图纸!', 1);
                    var _a = font1.calculateGeometry(text1, 1), vertices = _a.vertices, normals = _a.normals, uvs = _a.uvs, indices = _a.indices;
                    cube.geometry.positions = Array.from(vertices);
                    cube.geometry.normals = Array.from(normals);
                    cube.geometry.uvs = Array.from(uvs);
                    cube.geometry.indices = Array.from(indices);
                }
            });
        };
        script.src = './libs/opentype.min.js';
        document.head.appendChild(script);
    };
    GeometryFontTest.prototype.update = function () {
        this.cube.node3d.rx = 180;
        // this.cube.node3d.rx += 1;
        // log("this.cube.ry: " + this.cube.ry);
    };
    /**
     * 销毁
     */
    GeometryFontTest.prototype.dispose = function () {
        this.cube.dispose();
        this.cube = null;
    };
    return GeometryFontTest;
}(feng3d.Script));
function extractFontData(fontAll) {
    // get only the data we need in a better way
    var fontData = {
        glyphs: fontAll.glyphs,
        unitsPerEm: fontAll.unitsPerEm,
        familyName: fontAll['familyName'],
        ascender: fontAll.ascender,
        descender: fontAll.descender,
        tables: {
            name: fontAll.tables.name,
            post: {
                underlinePosition: fontAll.tables.post.underlinePosition,
                underlineThickness: fontAll.tables.post.underlineThickness
            },
            head: {
                yMin: fontAll.tables.head.yMin,
                xMin: fontAll.tables.head.xMin,
                yMax: fontAll.tables.head.yMax,
                xMax: fontAll.tables.head.xMax
            }
        },
        glyphsMap: {}
    };
    for (var i = 0; i < fontAll.glyphs.length; i++) {
        var glyph = fontAll.glyphs['glyphs'][i];
        if (glyph.unicode !== undefined) {
            fontData.glyphsMap[glyph.unicode] = glyph;
        }
    }
    return fontData;
}
function convert(font, restrict) {
    var result = {};
    result.glyphs = {};
    var restriction = {
        range: null,
        set: null
    };
    if (restrict) {
        var restrictContent = restrict;
        var rangeSeparator = '-';
        if (restrictContent.indexOf(rangeSeparator) !== -1) {
            var rangeParts = restrictContent.split(rangeSeparator);
            if (rangeParts.length === 2 && !isNaN(rangeParts[0]) && !isNaN(rangeParts[1])) {
                restriction.range = [parseInt(rangeParts[0]), parseInt(rangeParts[1])];
            }
        }
        if (restriction.range === null) {
            restriction.set = restrictContent;
        }
    }
    if (restriction.range) { // get characters from range, not use very often
        for (var i = 0; i < font.glyphs.length; i++) {
            var glyph = font.glyphs.glyphs[i];
            if (glyph.unicode !== undefined) {
                var glyphCharacter = String.fromCharCode(glyph.unicode);
                if ((glyph.unicode >= restriction.range[0] && glyph.unicode <= restriction.range[1])) {
                    result.glyphs[glyphCharacter] = fetchToken(glyph);
                }
            }
        }
    }
    else if (restriction.set) { // use quit a lot
        for (var _i = 0, _a = restriction.set; _i < _a.length; _i++) {
            var char = _a[_i];
            var charCode = char.codePointAt(0);
            var glyph = font.glyphsMap[charCode];
            if (glyph) {
                result.glyphs[char] = fetchToken(glyph);
            }
            else {
                console.warn("char: " + char + ", charCode: " + charCode);
            }
        }
    }
    else { // get all characters
        for (var i = 0; i < font.glyphs.length; i++) {
            var glyph = font.glyphs.glyphs[i];
            if (glyph.unicode !== undefined) {
                var glyphCharacter = String.fromCharCode(glyph.unicode);
                result.glyphs[glyphCharacter] = fetchToken(glyph);
            }
        }
    }
    result.familyName = font.familyName;
    result.ascender = Math.round(font.ascender);
    result.descender = Math.round(font.descender);
    result.underlinePosition = Math.round(font.tables.post.underlinePosition);
    result.underlineThickness = Math.round(font.tables.post.underlineThickness);
    result.boundingBox = {
        'yMin': Math.round(font.tables.head.yMin),
        'xMin': Math.round(font.tables.head.xMin),
        'yMax': Math.round(font.tables.head.yMax),
        'xMax': Math.round(font.tables.head.xMax)
    };
    result.unitsPerEm = font.unitsPerEm;
    result.original_font_information = font.tables.name;
    result.cssFontStyle = 'normal';
    return result;
}
function fetchToken(glyph) {
    var token = {};
    token.ha = Math.round(glyph.advanceWidth);
    token.x_min = Math.round(glyph.xMin);
    token.x_max = Math.round(glyph.xMax);
    token.o = '';
    glyph.path.commands.forEach(function (command, i) {
        if (command.type.toLowerCase() === 'c') {
            command.type = 'b';
        }
        token.o += command.type.toLowerCase();
        token.o += ' ';
        if (command.x !== undefined && command.y !== undefined) {
            token.o += Math.round(command.x);
            token.o += ' ';
            token.o += Math.round(command.y);
            token.o += ' ';
        }
        if (command.x1 !== undefined && command.y1 !== undefined) {
            token.o += Math.round(command.x1);
            token.o += ' ';
            token.o += Math.round(command.y1);
            token.o += ' ';
        }
        if (command.x2 !== undefined && command.y2 !== undefined) {
            token.o += Math.round(command.x2);
            token.o += ' ';
            token.o += Math.round(command.y2);
            token.o += ' ';
        }
    });
    return token;
}
var text1 = "\u56FA\u56FE\u8BF7\u8BBE\u8BA1\u65BD\u5DE5\u89C4\u8303\u603B\u8BF4\u660E\n\u4E00\u3001\t\u8BBE\u8BA1\u4F9D\u636E\n(1) \u5EFA\u8BBE\u65B9\u63D0\u4F9B\u7684\u5BA4\u5185\u8BBE\u8BA1\u8981\u6C42\u53CA\u5176\u4ED6\u76F8\u5173\u8D44\u6599\u3002\n(2)\u300A\u5EFA\u7B51\u5185\u90E8\u88C5\u4FEE\u8BBE\u8BA1\u9632\u706B\u89C4\u8303\u300B\uFF08GB-50022-2017\uFF09\u3002\n(3)\u300A\u5EFA\u7B51\u88C5\u9970\u88C5\u4FEE\u5DE5\u7A0B\u8D28\u91CF\u9A8C\u6536\u89C4\u8303\u300B\uFF08 GB50210-2018 \uFF09\u3002\n(4) \u56FD\u5BB6\u73B0\u884C\u7684\u6709\u5173\u89C4\u8303\u3001\u6807\u51C6\u548C\u89C4\u5B9A\u3002\u3001\n\n\u4E8C\u3001\u4E00\u822C\u8BF4\u660E\n\uFF081\uFF09\u672C\u56FE\u7EB8\u4E3AXXX\u8BBE\u8BA1\u65BD\u5DE5\u8BF4\u660E\u3002\n\uFF082\uFF09\u672C\u516C\u53F8\u8BBE\u8BA1\u6240\u6CE8\u88C5\u4FEE\u5C3A\u5BF8\u5355\u4F4D\u4E3A\u6BEB\u7C73\uFF08MM\uFF09\u3002\n\uFF083\uFF09\u51E1\u5C42\u697C\u5730\u9762\u6709\u5730\u6F0F\u5904\u7684\u627E\u5761\u53CA\u8303\u56F4\u5E94\u4EE5\u539F\u5EFA\u7B51\u8BBE\u8BA1\u4E3A\u51C6\u3002\n\uFF084\uFF09\u672C\u516C\u53F8\u6240\u9009\u7528\u7684\u4EA7\u54C1\u548C\u6750\u6599\u9700\u7B26\u5408\u56FD\u5BB6\u76F8\u5173\u7684\u8D28\u91CF\u68C0\u6D4B\u6807\u51C6\u3002\n\uFF085\uFF09\u6240\u6709\u88C5\u4FEE\u6750\u6599\u5747\u5E94\u91C7\u7528\u4E0D\u71C3\u6216\u96BE\u71C3\u6750\u6599\uFF0C\u6728\u6750\u5FC5\u987B\u91C7\u7528\u9632\u706B\u5904\u57CB\uFF0C\u57CB\u5165\u7ED3\u6784\u7684\u90E8\u5206\u5E94\u91C7\u7528\u9632\u8150\u5904\u7406\uFF0C\u7C7B\u4F3C\u7684\u6750\u6599\u5E94\u4E25\u683C\u6309\u7167\u56FD\u5BB6\u89C4\u8303\u8FDB\u884C\u5904\u7406\u3002\n\uFF086\uFF09\u5EFA\u7B51\u88C5\u4FEE\u65BD\u5DE5\u65F6\uFF0C\u9700\u4E0E\u5176\u4ED6\u5404\u5DE5\u79CD\u5BC6\u5207\u914D\u5408\uFF0C\u4E25\u683C\u9075\u5B88\u56FD\u5BB6\u9881\u5E03\u7684\u6709\u5173\u6807\u51C6\u53CA\u5404\u9879\u9A8C\u6536\u89C4\u8303\u3002\n\n\u4E09\u3001\u5EFA\u7B51\u88C5\u4FEE\u65BD\u5DE5\u6982\u51B5\n\uFF081\uFF09\u88C5\u4FEE\u6D89\u53CA\u4F7F\u7528\u7684\u88C5\u4FEE\u6750\u6599\u6709\u77F3\u6750\u3001\u5730\u7816\u3001\u6728\u6750\u3001\u77F3\u818F\u677F\u3001\u6D82\u6599\u3001\u6CB9\u6F06\u3001\u53CA\u591A\u79CD\u706F\u5177\u7B49\u3002(\u672C\u65B9\u6848\u6D89\u53CA\u4F7F\u7528\u6750\u6599\u8BE6\u89C1\u6750\u6599\u5355\uFF09\n\uFF082\uFF09\u672C\u5EFA\u7B51\u88C5\u4FEE\u7684\u505A\u6CD5\u9664\u6CE8\u660E\u4E4B\u5916\uFF0C\u5176\u5B83\u505A\u6CD5\u5747\u6309\u56FD\u5BB6\u7684\u6807\u51C6\u56FE\u96C6\u7684\u505A\u6CD5\u65BD\u5DE5\uFF0C\u5E76\u987B\u4E25\u683C\u9075\u5B88\u76F8\u5E94\u7684\u56FD\u5BB6\u9A8C\u6536\u89C4\u8303\u3002\n\n\u56DB\u3001\u56FE\u7EB8\u8F85\u52A9\u8BF4\u660E\n\uFF081\uFF09\u5BB6\u5177\u3001\u706F\u9970\u5728\u65BD\u5DE5\u56FE\u4E2D\u53EA\u4F5C\u793A\u610F\uFF0C\u5177\u4F53\u53C2\u8003\u5F62\u8C61\u56FE\u3002\n\uFF082\uFF09\u5927\u578B\u7684\u58C1\u9970\u3001\u88C5\u9970\u753B\u5DF2\u5728\u65BD\u5DE5\u56FE\u4E2D\u793A\u610F\uFF0C\u5177\u4F53\u5F85\u5B9A\u3002\n\uFF083\uFF09\u5DE5\u827A\u54C1\u7684\u9009\u62E9,\u5177\u4F53\u5F85\u5B9A\u3002\n\uFF084\uFF09\u5F3A\u4F53\u53CA\u95E8\u7A97\u6D1E\u53E3\u5C3A\u5BF8\u5B9A\u4F4D\uFF0C\u9664\u6807\u6CE8\u8005\u5916\uFF0C\u5747\u540C\u539F\u5EFA\u7B51\u8BBE\u8BA1\u56FE\u3002\n\n\u4E94\u3001\u4E3B\u8981\u6750\u6599\u53CA\u65BD\u5DE5\u5DE5\u827A\u8BF4\u660E\n\uFF08\u4E00\uFF09\u4E3B\u8981\u6750\u6599\u8BF4\u660E\uFF1A\n(1) \u8FDB\u53E3\u74F7\u7816\uFF0C\u78E8\u5149\u5EA6\u8FBE\u814A5\u5EA6\u4EE5\u4E0A\uFF0C\u539A\u5EA6\u8981\u57FA\u672C\u4E00\u81F4\uFF0C\u4EA7\u54C1\u8981\u9009\u7528\u201CA\u7EA7\u201D\u3002\u56FD\u4EA7\u82B1\u5C97\u77F3\uFF0C\u5927\u7406\u77F3\u7684\u4EA7\u54C1\u8D28\u91CF\u8981\u7B26\u5408\u56FD\u5BB6A\u7EA7\u4EA7\u54C1\u6807\u51C6\u3002\n\uFF082\uFF09\u8868\u9762\u88C5\u9970\u6728\u6599\uFF0C\u5C5E\u7B26\u5408\u56FD\u9645\u6807\u51C6\u7684A\u7EA7\u4EA7\u54C1\u3002\u6728\u65B9\uFF0C\u4E0D\u7BA1\u662F\u56FD\u4EA7\u8FD8\u662F\u8FDB\u53E3\uFF0C\u90FD\u9009\u7528\u4E0E\u8868\u9762\u9970\u677F\u76F8\u540C\u7EB9\u7406\u53CA\u76F8\u540C\u989C\u8272\u7684A\u7EA7\u4EA7\u54C1\uFF0C\u542B\u6C34\u7387\u8981\u63A7\u5236\u572815%\u4EE5\u5185\u3002\n\uFF083)\u4E73\u80F6\u6F06\u53CA\u805A\u5B89\u8102\u6F06\uFF0C\u5747\u4E3A\u5408\u8D44\u54D1\u5149\u6F06\uFF08\u4E2A\u522B\u5730\u65B9\u9664\u5916\uFF09\u3002\n\uFF084\uFF09\u5929\u82B1\u6750\u6599\uFF0C\u8F7B\u94A2\u9F99\u9AA8\u77F3\u818F\u677F\u5929\u82B1\uFF0C\u5747\u9009\u7528\u5408\u8D44\u9632\u706B\u9632\u6F6E\u7684\u4EA7\u54C1\u3002\n\uFF08\u4E8C\uFF09\u65BD\u5DE5\u5DE5\u827A\u8981\u6C42\uFF1A\uFF08\u6240\u6709\u65BD\u5DE5\u5FC5\u987B\u6309\u7167\u56FD\u5BB6\u65BD\u5DE5\u53CA\u9A8C\u6536\u89C4\u8303\u53CA\u76F8\u5E94\u7684\u4EA7\u54C1\u8BF4\u660E\u8FDB\u884C\u65BD\u5DE5\uFF09\n\uFF081\uFF09\u5F3A\u3001\u5730\u9762\n     \uFF08A\uFF09\u91C7\u7528\u629B\u5149\u7816\u6216\u629B\u91C9\u7816\u94FA\u8D34\u3002\u74F7\u7816\u65BD\u5DE5\u8981\u6C42\u4E25\u683C\u8FDB\u884C\u8BD5\u62FC\u6807\u53F7\uFF0C\u907F\u514D\u8272\u5DEE\u53CA\u7EB9\u8DEF\u51CC\u4E71\u4EE5\u4FDD\u8BC1\u89C6\u89C9\u6548\u679C\uFF0C\u540C\u65F6\u8981\u6C42\u9970\u9762\u5E73\u6574\uFF0C\u5782\u76F4\u6C34\u5E73\u5EA6\u597D\uFF0C\u7F1D\u7EBF\u7B14\u76F4\uFF0C\u63A5\u7F1D\u4E25\u5BC6\uFF0C\u65E0\u6C61\u67D3\u53CA\u53CD\u9508\u53CD\u78B1\uFF0C\u5E76\u65E0\u7A7A\u9F13\u7B49\u73B0\u8C61\u3002\u51E1\u662F\u767D\u8272\u6216\u8005\u6D45\u8272\u53A8\u536B\u5730\u677F\u74F7\u7816\uFF0C\u5728\u8D34\u524D\u90FD\u8981\u505A\u9632\u6C61\u53CA\u9632\u6D78\u900F\u5904\u7406\u3002\u51E1\u662F\u6728\u8D28\u5730\u677F\u94FA\u8BBE\uFF0C\u9700\u786E\u4FDD\u5730\u9762\u57FA\u5C42\u5E73\u6574\uFF0C\u518D\u884C\u94FA\u8BBE\u3002\n     \uFF08B\uFF09\u5730\u677F\u94FA\u8BBE\u8981\u6C42\u987B\u505A\u9632\u6F6E\uFF0C\u627E\u5E73\u5904\u7406\u5DF2\u8FBE\u5230\u94FA\u88C5\u8981\u6C42\u3002\n     \u5F3A\u5730\u7816\u94FA\u8D34\u8981\u53CA\u65F6\u6E05\u6D01\u7816\u9762\uFF0C\u4E0D\u53EF\u7A7A\u9F13\u5F00\u88C2,\u94FA\u8D34\u5B8C\u6BD5\u540E\u8981\u53CA\u65F6\u6E05\u7406\u7816\u7F1D\uFF0C\u586B\u5145\u586B\u7F1D\u5242\u6216\u767D\u6C34\u6CE5\u4FEE\u8865\u6240\u6709\u7F1D\u9699\n     \uFF08C\uFF09\u6240\u6709\u5916\u5F3A\u5185\u4FA7\u7684\u5F3A\u9762\uFF08\u6279\u6C34\u6CE5\u6216\u6728\u88C5\u9970\uFF09\u5747\u8981\u8FDB\u884C\u9632\u6C34\u5904\u7406\u3002\n      \u4EE5\u4E0A\u5DE5\u7A0B\u5E94\u6CE8\u610F\u540C\u5404\u4E13\u4E1A\u5B89\u88C5\u5DE5\u7A0B\u7684\u914D\u5408\uFF0C\u5C24\u5176\u9700\u540C\u4E13\u4E1A\u7684\u660E\u9732\u8BBE\u5907\uFF08\u5982\u7167\u660E\u63A7\u5236\u3001\u5F3A\u5F31\u7535\u63D2\u5EA7\u53CA\u63A7\u5236\u7B49\uFF09\u534F\u8C03\u65BD\u5DE5\uFF0C\u4EE5\u4FDD\u8BC1\u88C5\u4FEE\u6548\u679C\u3002\n\uFF082\uFF09\u5929\u82B1\n     \u6B64\u90E8\u5206\u5DE5\u7A0B\u4E5F\u5E94\u540C\u5404\u4E13\u4E1A\u65BD\u5DE5\u7684\u914D\u5408\uFF0C\u540A\u9876\u9970\u9762\u53CA\u55B7\u6D82\u9762\u5E94\u5E73\u6574\u5747\u5300\uFF0C\u98CE\u53E3\u3001\u97F3\u54CD\u53CA\u706F\u5177\u7B49\u5E94\u4E0E\u9876\u68DA\u8854\u63A5\u7D27\u5BC6\u5F97\u4F53\uFF0C\u6392\u5217\u6574\u9F50\uFF0C\u68C0\u67E5\u53E3\u5E94\u7EDF\u4E00\u89C4\u683C\uFF0C\u7ED3\u5408\u540A\u9876\u5185\u4E13\u4E1A\u7BA1\u7EBF\u7684\u60C5\u51B5\u5408\u7406\u5E03\u7F6E\u3002\n\uFF083\uFF09\u95E8\u7A97\n     \u8BE6\u89C1\u65BD\u5DE5\u56FE\u3002\n\uFF084\uFF09\u5BB6\u5177\n   \uFF08A\uFF09\u56FA\u5B9A\u5BB6\u5177\u8BF7\u53C2\u7167\u8BE6\u56FE\uFF0C\u5177\u4F53\u5C3A\u5BF8\u5C31\u4F9D\u636E\u73B0\u573A\u5B9E\u9645\u786E\u5B9A\u3002\n   \uFF08B\uFF09\u536B\u751F\u95F4\u6D01\u5177\u53C2\u8003\u5F62\u8C61\u56FE\u3002\n\uFF085\uFF09\u706F\u5177\n\u706F\u5177\u5B89\u88C5\u5E94\u6392\u5217\u6574\u9F50\uFF0C\u5E03\u7F6E\u5747\u5300\uFF0C\u67D0\u4E9B\u573A\u5408\u5982\u9700\u4E13\u4E1A\u8BBE\u8BA1\u5E94\u7ED3\u5408\u8BBE\u8BA1\u7684\u98CE\u683C\u8FDB\u884C\u5904\u7406\u3002\n\u516D\u3001\u4E13\u4E1A  \u8981\u6C42\n\uFF081\uFF09\u7A7A\u8C03\u6696\u901A\u7CFB\u7EDF\uFF1A\u7A7A\u8C03\u6696\u901A\u7CFB\u7EDF\u540C\u539F\u5EFA\u7B51\u8BBE\u8BA1\u3002\n\uFF082\uFF09\u5F3A\u5F31\u7535\u7CFB\u7EDF\uFF1A\u5F00\u5173\u3001\u63D2\u5EA7\u3001\u62A5\u8B66\u5668\u660E\u9732\u4EF6\u7684\u6837\u5F0F\u989C\u8272\u5E94\u4E0E\u5185\u534F\u8C03\u7EDF\u4E00\u5E76\u6392\u5217\u6574\u9F50\u3002\n\u4E03\u3001\u6240\u6709\u505A\u6CD5\u5747\u4EE5\u8BE6\u56FE\u4E3A\u51C6\u3002\n\u516B\u3001\u5DE5\u7A0B\u65BD\u5DE5\u5FC5\u987B\u4E25\u683C\u6309\u7167\u4E2D\u534E\u4EBA\u6C11\u5171\u548C\u56FD\u73B0\u6709\u7684\u65BD\u5DE5\u9A8C\u6536\u89C4\u8303\u6267\u884C\uFF0C\u5404\u5DE5\u79CD\u76F8\u4E92\u534F\u8C03\u914D\u5408\u3002\n\u4E5D\u3001\u56FE\u4E2D\u82E5\u6709\u5C3A\u5BF8\u4E0E\u73B0\u72B6\u53CA\u8BBE\u8BA1\u6548\u679C\u77DB\u76FE\u4E4B\u5904\uFF0C\u8BBE\u8BA1\u5E08\u53EF\u6839\u636E\u73B0\u573A\u60C5\u51B5\u9002\u5F53\u8C03\u6574\u3002\n\n\u51FA\u56FE\u65B9LOGO\n\nINTERIOR DESIGN\n\u5BA4\u5185\u88C5\u9970\u8BBE\u8BA1\u5355\u4F4D\n\nPROJECT NO.\n\u5DE5\u7A0B\u7F16\u53F7\n\nNOTE\n\u6CE8\u610F\n\u6240\u6709\u5C3A\u5BF8\u4EE5\u73B0\u573A\u5B9E\u9645\u6D4B\u91CF\u4E3A\u51C6\uFF0C\u8BF7\u52FF\u6309\u6BD4\u4F8B\u6D4B\u91CF\u56FE\u7EB8\uFF0C\u6240\u6709\u6CE8\u91CA\u53CA\u8BF4\u660E\u89C1\u56FE\u7EB8\u6807\u6CE8\u3002\u7248\u6743\u6240\u6709\uFF0C\u672A\u7ECF\u6388\u6743\uFF0C\u4E0D\u5F97\u64C5\u81EA\u4F7F\u7528\u672C\u56FE\u548C\u8BBE\u8BA1\u3002\u73B0\u573A\u4E0E\u672C\u56FE\u7EB8\u4E0D\u4E00\u81F4\u4E4B\u5904\u5E94\u5728\u65BD\u5DE5\u524D\u4E66\u9762\u544A\u77E5\u8BBE\u8BA1\u65B9\u3002\u672A\u7B7E\u7AE0\u56FE\u7EB8\u4E0D\u5F97\u4F5C\u4E3A\u65BD\u5DE5\u7528\u9014\uFF0C\u672A\u7ECF\u8BB8\u53EF\u64C5\u81EA\u4F7F\u7528\u56FE\u7EB8\u6307\u5BFC\u65BD\u5DE5\uFF0C\u539F\u8BBE\u8BA1\u65B9\u4E0D\u8D1F\u8D23\u4EFB\u4F55\u540E\u679C\u3002\n\nSPECIAL SEAL OUT THE MAP\n\u51FA\u56FE\u4E13\u7528\u7AE0\n\nCLIENT\n\u4E1A\u4E3B\n\nPROJECT NAME\n\u9879\u76EE\u540D\u79F0\n\nSHEET TITLE\n\u56FE\u540D\n\n\u8BBE\u8BA1\u603B\u8BF4\u660E\n\nSTAGE\n\u9636\u6BB5\n\n\u65B9\u6848/\u65BD\u5DE5\u56FE\nPROFESSIONAL\n\u4E13\u4E1A\n\nDESIGN\n\u8BBE\u8BA1\n\nDRAWBY\n\u7ED8\u56FE\n\nAUDIT\n\u5BA1\u6838\n\nSCALE\n\u6BD4\u4F8B\n\nDATE\n\u65E5\u671F\n\nDRAWING NO.\n\u56FE\u7EB8\u7F16\u53F7\n";
//# sourceMappingURL=index.js.map