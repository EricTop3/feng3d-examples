class Basic_SkyBox extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var cubeTexture = feng3d.serialization.setValue(new feng3d.TextureCube(), {
            rawData: {
                type: "path", paths: [
                    'resources/skybox/snow_positive_x.jpg',
                    'resources/skybox/snow_positive_y.jpg',
                    'resources/skybox/snow_positive_z.jpg',
                    'resources/skybox/snow_negative_x.jpg',
                    'resources/skybox/snow_negative_y.jpg',
                    'resources/skybox/snow_negative_z.jpg',
                ]
            }
        });

        var skybox = feng3d.serialization.setValue(new feng3d.Entity(), { name: "skybox" }).addComponent(feng3d.SkyBox);
        skybox.s_skyboxTexture = cubeTexture;
        scene.node3d.addChild(skybox.node3d);

        camera.node3d.z = -6;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.lens = new feng3d.PerspectiveLens(90);

        var torusMaterial = feng3d.serialization.setValue(new feng3d.Material(), { uniforms: { s_envMap: cubeTexture } });
        // torusMaterial.uniforms.u_specular.a = 0.5;
        // torusMaterial.uniforms.u_ambient.fromUnit(0x111111);
        // torusMaterial.uniforms.u_ambient.a = 0.25;

        var torus = feng3d.serialization.setValue(new feng3d.Entity(), { name: "torus" }).addComponent(feng3d.Renderable);
        torus.geometry = feng3d.serialization.setValue(new feng3d.TorusGeometry(), { radius: 1.50, tubeRadius: 0.60, segmentsR: 40, segmentsT: 20 });
        torus.material = torusMaterial;
        scene.node3d.addChild(torus.node3d);

        feng3d.ticker.onframe(() =>
        {
            torus.node3d.rx += 2;
            torus.node3d.ry += 1;
            camera.node3d.x = 0;
            camera.node3d.y = 0;
            camera.node3d.z = 0;
            camera.node3d.ry += 0.5 * (feng3d.windowEventProxy.clientX - canvas.clientLeft - canvas.clientWidth / 2) / 800;
            camera.node3d.moveBackward(6);
        });
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}