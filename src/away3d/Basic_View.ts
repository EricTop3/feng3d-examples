class Basic_View extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");


        camera.node3d.z = -6;
        camera.node3d.y = 5;
        camera.node3d.lookAt(new feng3d.Vector3());

        var plane = new feng3d.Entity().addComponent(feng3d.Renderable);
        plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 7, height: 7 });
        var material = plane.material = feng3d.serialization.setValue(new feng3d.Material(), { uniforms: { s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_diffuse.jpg" } } } });
        scene.node3d.addChild(plane.node3d);

        feng3d.ticker.onframe(() =>
        {
            plane.node3d.ry += 1;
        });
    }

    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}