class Basic_Shading extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var planeMaterial: feng3d.Material;
        var sphereMaterial: feng3d.Material;
        var cubeMaterial: feng3d.Material;
        var torusMaterial: feng3d.Material;
        var light1: feng3d.DirectionalLight;
        var light2: feng3d.DirectionalLight;
        var plane: feng3d.Renderable;
        var sphere: feng3d.Renderable;
        var cube: feng3d.Renderable;
        var torus: feng3d.Renderable;

        initEngine();
        initLights();
        initMaterials();
        initObjects();
        initListeners();

        function initEngine()
        {

            camera.node3d.y = 5;
            camera.node3d.z = -10;
            camera.node3d.lookAt(new feng3d.Vector3());
            camera.node3d.addComponent(feng3d.FPSController);
        }

        function initMaterials()
        {
            planeMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/floor_specular.jpg" } },
                }
            });
            sphereMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/beachball_diffuse.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/beachball_specular.jpg" } },
                }
            });
            cubeMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/trinket_specular.jpg" } },
                }
            });
            torusMaterial = feng3d.serialization.setValue(new feng3d.Material(), {
                shaderName: "standard", uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_diffuse.jpg" } },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_normal.jpg" } },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: "resources/weave_diffuse.jpg" } },
                }
            });
        }

        function initLights()
        {
            scene.ambientColor.a = 0.2;

            light1 = new feng3d.Entity().addComponent(feng3d.DirectionalLight);
            light1.intensity = 0.7;
            light1.node3d.rx = 90;
            scene.node3d.addChild(light1.node3d);

            light2 = new feng3d.Entity().addComponent(feng3d.DirectionalLight);
            light2.color.fromUnit(0x00FFFF);
            light2.intensity = 0.7;
            light2.node3d.rx = 90;
            scene.node3d.addChild(light2.node3d);
        }

        function initObjects()
        {
            plane = new feng3d.Entity().addComponent(feng3d.Renderable);
            var geometry: feng3d.Geometry = plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 10, height: 10 });
            plane.material = planeMaterial;
            geometry.scaleU = 2;
            geometry.scaleV = 2;
            plane.node3d.y = -0.20;
            scene.node3d.addChild(plane.node3d);
            sphere = new feng3d.Entity().addComponent(feng3d.Renderable);
            sphere.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 1.50, segmentsW: 40, segmentsH: 20 })
            sphere.material = sphereMaterial;
            sphere.node3d.x = 3;
            sphere.node3d.y = 1.60;
            sphere.node3d.z = 3.00;
            scene.node3d.addChild(sphere.node3d);
            cube = new feng3d.Entity().addComponent(feng3d.Renderable);
            cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 2, height: 2, depth: 2, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
            cube.material = cubeMaterial;
            cube.node3d.x = 3.00;
            cube.node3d.y = 1.60;
            cube.node3d.z = -2.50;
            scene.node3d.addChild(cube.node3d);
            torus = new feng3d.Entity().addComponent(feng3d.Renderable);
            geometry = torus.geometry = feng3d.serialization.setValue(new feng3d.TorusGeometry(), { radius: 1.50, tubeRadius: 0.60, segmentsR: 40, segmentsT: 20 });
            torus.material = torusMaterial;
            geometry.scaleU = 10;
            geometry.scaleV = 5;
            torus.node3d.x = -2.50;
            torus.node3d.y = 1.60;
            torus.node3d.z = -2.50;
            scene.node3d.addChild(torus.node3d);
        }

        function initListeners()
        {
            feng3d.ticker.onframe(onEnterFrame, this);
        }

        function onEnterFrame()
        {
            light1.node3d.rx = 30;
            light1.node3d.ry++;
        }
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}