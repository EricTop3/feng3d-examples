class SkyBoxTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];

        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);
        //

        var skybox = feng3d.serialization.setValue(new feng3d.Entity(), { name: "skybox" }).addComponent(feng3d.SkyBox);
        skybox.s_skyboxTexture = feng3d.serialization.setValue(new feng3d.TextureCube(), {
            rawData: {
                type: "path", paths: [
                    'resources/skybox/px.jpg',
                    'resources/skybox/py.jpg',
                    'resources/skybox/pz.jpg',
                    'resources/skybox/nx.jpg',
                    'resources/skybox/ny.jpg',
                    'resources/skybox/nz.jpg'
                ]
            }
        }
        );
        scene.node3d.addChild(skybox.node3d);
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}