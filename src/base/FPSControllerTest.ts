class FPSControllerTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];

        var cube = feng3d.Entity.createPrimitive("Cube");
        this.node3d.addChild(cube);

        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        this.node3d.addChild(sphere);

        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        this.node3d.addChild(capsule);

        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        this.node3d.addChild(cylinder);

        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        //
        camera.node3d.addComponent(feng3d.FPSController);
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}