class FogTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var model = new feng3d.Entity().addComponent(feng3d.Renderable);
        model.node3d.z = -7;
        model.node3d.y = 0;
        this.node3d.addChild(model.node3d);

        model.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        //材质
        var material = model.material = feng3d.serialization.setValue(new feng3d.Material(), {
            uniforms: {
                s_diffuse: {
                    __class__: "feng3d.Texture2D",
                    source: { url: 'resources/m.png' }
                },
                u_fogMode: feng3d.FogMode.LINEAR,
                u_fogColor: new feng3d.Color3(1, 1, 0),
                u_fogMinDistance: 2,
                u_fogMaxDistance: 3,
            }
        });


        feng3d.ticker.onframe(() =>
        {
            model.node3d.ry += 1;
        });
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
     * 销毁时调用
     */
    dispose()
    {

    }
}