class MousePickTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {

        /**
         * 操作方式:鼠标按下后可以使用移动鼠标改变旋转，wasdqe平移
         */
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];

        camera.node3d.z = -5;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);

        var cube = feng3d.Entity.createPrimitive("Cube");
        cube.mouseEnabled = true;
        cube.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(cube);

        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        sphere.mouseEnabled = true;
        sphere.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(sphere);

        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        capsule.mouseEnabled = true;
        capsule.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(capsule);

        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        cylinder.mouseEnabled = true;
        cylinder.getComponent(feng3d.Renderable).material = new feng3d.Material();
        scene.node3d.addChild(cylinder);

        scene.on("click", (event) =>
        {
            var entity = <feng3d.Entity>event.target;
            if (entity.getComponent(feng3d.Renderable))
            {
                var uniforms = <feng3d.StandardUniforms>entity.getComponent(feng3d.Renderable).material.uniforms;
                uniforms.u_diffuse.fromUnit(Math.random() * (1 << 24));
            }
        });

        // var engines = feng3d.Feng3dObject.getObjects(feng3d.Engine);

        // engines[0].mouse3DManager.mouseInput.catchMouseMove = true;

        // scene.on("mouseover", (event) =>
        // {
        //     var gameObject = <feng3d.Entity>event.target;
        //     if (gameObject.getComponent(feng3d.Renderable))
        //     {
        //         var uniforms = <feng3d.StandardUniforms>gameObject.getComponent(feng3d.Renderable).material.uniforms;
        //         uniforms.u_diffuse.setTo(0, 1, 0);
        //     }
        // });

        // scene.on("mouseout", (event) =>
        // {
        //     var gameObject = <feng3d.Entity>event.target;
        //     if (gameObject.getComponent(feng3d.Renderable))
        //     {
        //         var uniforms = <feng3d.StandardUniforms>gameObject.getComponent(feng3d.Renderable).material.uniforms;
        //         uniforms.u_diffuse.setTo(1, 1, 1);
        //     }
        // });
    }

    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}