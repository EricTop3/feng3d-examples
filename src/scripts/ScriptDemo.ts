class ScriptDemo extends feng3d.Script
{
    cube: feng3d.Renderable;

    init()
    {
        var cube = this.cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = -7;
        this.node3d.addChild(cube.node3d);

        cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        //材质
        var material = cube.material = new feng3d.Material();
        var uniforms = <feng3d.StandardUniforms>material.uniforms;
        uniforms.s_diffuse = new feng3d.Texture2D();
        uniforms.s_diffuse.source = { url: 'resources/m.png' };

        uniforms.u_fogMode = feng3d.FogMode.LINEAR;
        uniforms.u_fogColor = new feng3d.Color3(1, 1, 0);
        uniforms.u_fogMinDistance = 2;
        uniforms.u_fogMaxDistance = 3;
    }

    update()
    {
        this.cube.node3d.ry += 1;
        // log("this.cube.ry: " + this.cube.ry);
    }

    /**
     * 销毁
     */
    dispose()
    {
        this.cube.dispose();
        this.cube = null;
    }
}