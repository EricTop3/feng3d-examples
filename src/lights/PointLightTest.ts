class PointLightTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var light0 = feng3d.serialization.setValue(new feng3d.Entity(), { name: "pointLight" }).addComponent(feng3d.Renderable);
        var light1 = feng3d.serialization.setValue(new feng3d.Entity(), { name: "pointLight" }).addComponent(feng3d.Renderable);

        initObjects();
        initLights();

        feng3d.ticker.onframe(setPointLightPosition);

        camera.node3d.z = -5;
        camera.node3d.y = 2;
        camera.node3d.lookAt(new feng3d.Vector3());
        camera.node3d.addComponent(feng3d.FPSController);
        //
        feng3d.windowEventProxy.on("keyup", (event) =>
        {
            var boardKey = String.fromCharCode(event.data.keyCode).toLocaleLowerCase();
            switch (boardKey)
            {
                case "c":
                    clearObjects();
                    break;
                case "b":
                    initObjects();
                    scene.node3d.addChild(light0.node3d);
                    scene.node3d.addChild(light1.node3d);
                    break;
            }
        });

        function initObjects()
        {
            var material = feng3d.serialization.setValue(new feng3d.Material(), {
                uniforms: {
                    s_diffuse: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_diffuse.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                    s_normal: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_normals.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                    s_specular: { __class__: "feng3d.Texture2D", source: { url: 'resources/head_specular.jpg' }, wrapS: feng3d.TextureWrap.MIRRORED_REPEAT, wrapT: feng3d.TextureWrap.MIRRORED_REPEAT },
                }
            });

            //初始化立方体
            var plane = new feng3d.Entity().addComponent(feng3d.Renderable);
            plane.node3d.y = -1;
            var geometry = plane.geometry = feng3d.serialization.setValue(new feng3d.PlaneGeometry(), { width: 10, height: 10 });
            geometry.scaleU = 2;
            geometry.scaleV = 2;
            plane.material = material;
            scene.node3d.addChild(plane.node3d);

            var cube = new feng3d.Entity().addComponent(feng3d.Renderable);
            cube.material = material;
            cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
            cube.geometry.scaleU = 2;
            cube.geometry.scaleV = 2;
            scene.node3d.addChild(cube.node3d);
        }

        function clearObjects()
        {
            for (var i = scene.node3d.numChildren - 1; i >= 0; i--)
            {
                scene.node3d.removeChildAt(i);
            }
        }

        function initLights()
        {
            //
            var lightColor0 = new feng3d.Color4(1, 0, 0, 1);
            light0.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 0.05 });
            //初始化点光源
            var pointLight0 = light0.addComponent(feng3d.PointLight);
            pointLight0.color = lightColor0.toColor3();
            light0.material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color", uniforms: { u_diffuseInput: lightColor0 } });
            scene.node3d.addChild(light0.node3d);

            //
            var lightColor1 = new feng3d.Color4(0, 1, 0, 1);
            light1.geometry = feng3d.serialization.setValue(new feng3d.SphereGeometry(), { radius: 0.05 });
            //初始化点光源
            var pointLight1 = light1.addComponent(feng3d.DirectionalLight);
            pointLight1.color = lightColor1.toColor3();
            light1.material = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "color", uniforms: { u_diffuseInput: lightColor1 } });
            scene.node3d.addChild(light1.node3d);
        }

        function setPointLightPosition()
        {
            var time = new Date().getTime();
            //
            var angle = time / 1000;
            light0.node3d.x = Math.sin(angle) * 3;
            light0.node3d.z = Math.cos(angle) * 3;
            //
            angle = angle + Math.PI / 2;
            light1.node3d.x = Math.sin(angle) * 3;
            light1.node3d.z = Math.cos(angle) * 3;
            light1.node3d.lookAt(new feng3d.Vector3());
        }
    }

    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}