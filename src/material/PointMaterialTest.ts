class PointMaterialTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var pointGeometry = new feng3d.PointGeometry();
        var pointMaterial = feng3d.serialization.setValue(new feng3d.Material(), { shaderName: "point", renderParams: { renderMode: feng3d.RenderMode.POINTS } });
        var model = feng3d.serialization.setValue(new feng3d.Entity(), { name: "plane" }).addComponent(feng3d.Renderable);
        model.geometry = pointGeometry;
        model.material = pointMaterial;
        model.node3d.z = 3;
        scene.node3d.addChild(model.node3d);

        var length = 200;
        var height = 2 / Math.PI;
        for (var x = -length; x <= length; x = x + 4)
        {
            var angle = x / length * Math.PI;
            var vec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
            var pointInfo = new feng3d.PointInfo();
            pointInfo.position = vec;
            pointGeometry.points.push(pointInfo);
        }

        //变化旋转
        setInterval(function ()
        {
            model.node3d.ry += 1;
            (<feng3d.PointUniforms>pointMaterial.uniforms).u_PointSize = 1 + 5 * Math.sin(model.node3d.ry / 30);
        }, 15);
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}