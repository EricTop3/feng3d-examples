class SegmentMaterialTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var segment = feng3d.serialization.setValue(new feng3d.Entity(), { name: "segment" }).addComponent(feng3d.Renderable);
        segment.node3d.z = 3;
        scene.node3d.addChild(segment.node3d);

        //初始化材质
        segment.material = feng3d.Material.getDefault("Segment-Material");
        var segmentGeometry = segment.geometry = new feng3d.SegmentGeometry();

        var length = 200;
        var height = 2 / Math.PI;
        var preVec: feng3d.Vector3;
        for (var x = -length; x <= length; x++)
        {
            var angle = x / length * Math.PI;
            if (preVec == null)
            {
                preVec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
            } else
            {
                var vec = new feng3d.Vector3(x / 100, Math.sin(angle) * height, 0);
                segmentGeometry.addSegment({ start: preVec, end: vec });
                preVec = vec;
            }
        }

        //变化旋转
        setInterval(function ()
        {
            segment.node3d.ry += 1;
        }, 15);
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}