class StandardMaterialTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");

        var cube = new feng3d.Entity().addComponent(feng3d.Renderable);
        cube.node3d.z = 3;
        cube.node3d.y = -1;
        scene.node3d.addChild(cube.node3d);

        //变化旋转与颜色
        setInterval(function ()
        {
            cube.node3d.ry += 1;
        }, 15);

        cube.geometry = feng3d.serialization.setValue(new feng3d.CubeGeometry(), { width: 1, height: 1, depth: 1, segmentsW: 1, segmentsH: 1, segmentsD: 1, tile6: false });
        // model.geometry = new PlaneGeometry();
        //材质
        var textureMaterial = cube.material = new feng3d.Material();
        var uniforms = <feng3d.StandardUniforms>textureMaterial.uniforms;
        uniforms.s_diffuse = new feng3d.Texture2D();
        uniforms.s_diffuse.source = { url: 'resources/m.png' };
        // textureMaterial.uniforms.s_diffuse.url = 'resources/nonpowerof2.png';
        uniforms.s_diffuse.format = feng3d.TextureFormat.RGBA;
        // textureMaterial.diffuseMethod.alphaThreshold = 0.1;

        uniforms.s_diffuse.anisotropy = 16;
        uniforms.u_diffuse.a = 0.2;

        textureMaterial.renderParams.enableBlend = true;
    }

    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}