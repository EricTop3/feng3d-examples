class PrimitiveTest extends feng3d.Script
{
    /**
     * 初始化时调用
     */
    init()
    {
        var scene = this.node3d.scene;;
        var camera = scene.getComponentsInChildren(feng3d.Camera)[0];
        var canvas = document.getElementById("glcanvas");


        var cube = feng3d.Entity.createPrimitive("Cube");
        this.node3d.addChild(cube);

        var plane = feng3d.Entity.createPrimitive("Plane");
        plane.x = 1.50;
        plane.rx = -90;
        plane.sx = 0.1;
        plane.sy = 0.1;
        plane.sz = 0.1;
        this.node3d.addChild(plane);

        var sphere = feng3d.Entity.createPrimitive("Sphere");
        sphere.x = -1.50;
        this.node3d.addChild(sphere);

        var capsule = feng3d.Entity.createPrimitive("Capsule");
        capsule.x = 3;
        this.node3d.addChild(capsule);

        var cylinder = feng3d.Entity.createPrimitive("Cylinder");
        cylinder.x = -3;
        this.node3d.addChild(cylinder);

        var controller = new feng3d.LookAtController(camera.node3d);
        controller.lookAtPosition = new feng3d.Vector3();
        //
        setInterval(() =>
        {
            var time = new Date().getTime();
            var angle = (Math.round(time / 17) % 360);
            angle = angle * Math.DEG2RAD;
            camera.node3d.x = 10 * Math.sin(angle);
            camera.node3d.y = 0;
            camera.node3d.z = 10 * Math.cos(angle);
            controller.update();
        }, 17);
    }
    /**
     * 更新
     */
    update()
    {
    }

    /**
    * 销毁时调用
    */
    dispose()
    {

    }
}